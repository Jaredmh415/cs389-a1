Ahir, Jared, Jack

Interactable Assignment
========================
Ahir - Pendulum/Spring
    For the pendulum interactable, we roughly followed a guide from here: https://www.youtube.com/watch?v=y2wfX1Ud4uk. That being said, we decided that we didn't want to do it all in blueprint, and therefore made the components in C++, and added an impulse on a timer that allowed it to swing back and forth in the level.
Jared - Teleport
    For the teleporter, we created two actors in order to actually make the object teleport, one with a visible mesh component and one without one, rendering it invisible in the level. We put it on a 7 second timer in a similar manner to the pendulum, so that way it could only be used every 7 seconds like specified.
Jack - Movement
    For the movement interactable, we followed a similar ideation for the pendulum interactable, while also using impulses from the force application pawn to sporadically move around the level with different force vectors. This was also on a timer, and alternated between different vector directions to make it move "randomly".

Pawn Assignment/Arena
========================
Ahir - Destroy pawn
    This pawn is similar to the behavior in lab 3, in where it checks all overlapping actors with it's hitbox and then destroys any interactables by looking at the actor's class and then if it matches a type, calling destroy on it.
Jack - Arena + Kill-Z
    The arena was made to look like a patch of different floors/tiles, and then had multiple lighting sources from above as well. It had tall walls, and then above that a Kill-Z for players that managed to get above it, causing the level to reset.
Jared - Force Application pawn
    This pawn has behavior where it simply applies a force vector in a fixed direction to any interactable it touches (as long as they have physics on). This was our original pawn, and the movement for the destroy pawn heavily relies on this pawn as well.

Screenshots/Demo:
=======================
Check the gitlab repository for all of the screenshots, and the demo link is here:
