// Fill out your copyright notice in the Description page of Project Settings.


#include "TeleportPad.h"

// Sets default values
ATeleportPad::ATeleportPad()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    //Components
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
    MeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

}

// Called when the game starts or when spawned
void ATeleportPad::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATeleportPad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

