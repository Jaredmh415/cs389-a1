// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "ForceApplicationPawn.generated.h"

UCLASS()
class CS378_A1_API AForceApplicationPawn : public APawn
{
	GENERATED_BODY()
    
    /** The camera */
    UPROPERTY(Category = Camera, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
    class UCameraComponent* CameraComponent;

    /** Camera boom positioning the camera above the character */
    UPROPERTY(Category = Camera, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
    class USpringArmComponent* CameraBoom;

public:
	// Sets default values for this pawn's properties
	AForceApplicationPawn();
    
    FORCEINLINE class UStaticMeshComponent *  GetMeshComponent() const {return MeshComponent;}
    FORCEINLINE class UBoxComponent * GetHitboxComponent() const {return HitboxComponent;}
    
    UFUNCTION(BlueprintImplementableEvent)
    void InteractPressed();
    
    UFUNCTION(BlueprintImplementableEvent)
    void InteractPressedUp();
    
    static const FName ForceImpulse;
    static const FName ForceImpulseUp;
    /* The speed our ship moves around the level */
    UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
    float ImpulseScalar;
    
    UFUNCTION(BlueprintCallable)
    void PerformInteraction();
    
    UFUNCTION(BlueprintCallable)
    void PerformInteractionUp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UStaticMeshComponent *MeshComponent;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UBoxComponent *HitboxComponent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
