// Fill out your copyright notice in the Description page of Project Settings.


#include "DestructivePawn.h"
#include "TimerManager.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/CollisionProfile.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "Components/BoxComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "GameFramework/Actor.h"
#include "Engine/StaticMesh.h"
#include "TeleportActor.h"
#include "PendulumActor.h"
#include "MovingInteractable.h"

const FName ADestructivePawn::ForceImpulse("ForceImpulse");

// Sets default values
ADestructivePawn::ADestructivePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;

	HitboxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("HitboxComponent"));
	HitboxComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when ship does
	CameraBoom->TargetArmLength = 1200.f;
	CameraBoom->SetRelativeRotation(FRotator(-80.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = false;    // Camera does not rotate relative to arms

	//Components will be edited in blueprint as well

	ImpulseScalar = 1000.0f;

}

// Called when the game starts or when spawned
void ADestructivePawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADestructivePawn::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);
}

void ADestructivePawn::PerformInteraction()
{
	const FVector forward = GetActorForwardVector();
	UStaticMeshComponent* SM = Cast<UStaticMeshComponent>(this->MeshComponent);
	SM->AddImpulse(forward * SM->GetMass() * 1000);

}

// Called to bind functionality to input
void ADestructivePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction(ForceImpulse, IE_Pressed, this, &ADestructivePawn::InteractPressed);
}

void ADestructivePawn::DestroyInteractable() {
	TArray< AActor*>OverlappingActors;
	HitboxComponent->GetOverlappingActors(OverlappingActors);
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Purple, FString::Printf(TEXT("%d"), OverlappingActors.Num()));
	}
	for (AActor* actor : OverlappingActors)
	{
		if (actor->IsA(APendulumActor::StaticClass()) || actor->IsA(ATeleportActor::StaticClass()) || actor->IsA(AMovingInteractable::StaticClass())) {
			actor->Destroy();
		}
	}

}

