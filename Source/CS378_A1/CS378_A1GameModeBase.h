// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CS378_A1GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CS378_A1_API ACS378_A1GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
    
public:
    ACS378_A1GameModeBase();
	
};
