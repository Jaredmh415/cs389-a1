// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PendulumActor.generated.h"

UCLASS()
class CS378_A1_API APendulumActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APendulumActor();

	FORCEINLINE UStaticMeshComponent* GetMeshComponent() const { return MeshComponent; }
	FORCEINLINE UStaticMeshComponent* GetSwingComponent() const { return SwingComponent; }

	UFUNCTION(BlueprintCallable)
		void Swing();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	int swings;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* MeshComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* SwingComponent;
	FTimerHandle PendulumTimeManager;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
