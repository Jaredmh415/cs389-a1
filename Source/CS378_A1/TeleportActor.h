// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TeleportPad.h"
#include "TimerManager.h"
#include "TeleportActor.generated.h"

UCLASS()
class CS378_A1_API ATeleportActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATeleportActor();
    
    FORCEINLINE class UStaticMeshComponent *  GetMeshComponent() const {return MeshComponent;}

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UStaticMeshComponent *MeshComponent;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    ATeleportPad *teleportPad;
    
    
    /* Handle to manage the timer */
    FTimerHandle TeleportTimerHandler;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
    
    UFUNCTION(BlueprintCallable)
    void Teleport();

};
