// Fill out your copyright notice in the Description page of Project Settings.


#include "TimerManager.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "MovingInteractable.h"

// Sets default values
AMovingInteractable::AMovingInteractable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
    RootComponent = MeshComponent;
   
}

// Called when the game starts or when spawned
void AMovingInteractable::BeginPlay()
{
	Super::BeginPlay();
    GetWorldTimerManager().SetTimer(MovingTime, this, &AMovingInteractable::MoveInteractable, 3.0f, true, 1.0f);
    direction = 0;
    
    
	
}

// Called every frame
void AMovingInteractable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMovingInteractable::MoveInteractable()
{
    FVector directionVector;
    if (direction%4 == 0)
    {
        directionVector = GetActorForwardVector();
    } else if (direction %4 == 1)
    {
        directionVector = GetActorUpVector();
    } else if(direction%4 == 2)
    {
        directionVector = GetActorRightVector();
    } else
    {
        directionVector = GetActorRightVector() * -1;
    }
    
    
    
    UStaticMeshComponent* SM = Cast<UStaticMeshComponent>(this->MeshComponent);    
    SM->AddImpulse(directionVector * SM->GetMass() * 600);
    direction+=1;
}

