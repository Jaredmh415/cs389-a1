// Fill out your copyright notice in the Description page of Project Settings.



#include "ForceApplicationPawn.h"
#include "TimerManager.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/CollisionProfile.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "Components/BoxComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "GameFramework/Actor.h"
#include "Engine/StaticMesh.h"

const FName AForceApplicationPawn::ForceImpulse("ForceImpulse");
const FName AForceApplicationPawn::ForceImpulseUp("ForceImpulseUp");


// Sets default values
AForceApplicationPawn::AForceApplicationPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
    RootComponent = MeshComponent;
    
    
    HitboxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("HitboxComponent"));
    HitboxComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
    // Movement
    ImpulseScalar = 1000.0f;
    
    
    
    // Create a camera boom...
    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->SetupAttachment(RootComponent);
    CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when ship does
    CameraBoom->TargetArmLength = 1200.f;
    CameraBoom->SetRelativeRotation(FRotator(-80.f, 0.f, 0.f));
    CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

    // Create a camera...
    CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
    CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
    CameraComponent->bUsePawnControlRotation = false;    // Camera does not rotate relative to arm

}

// Called when the game starts or when spawned
void AForceApplicationPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AForceApplicationPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AForceApplicationPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
//	Super::SetupPlayerInputComponent(PlayerInputComponent);
    check(PlayerInputComponent);
    PlayerInputComponent->BindAction(ForceImpulse, IE_Pressed, this, &AForceApplicationPawn::InteractPressed);
    PlayerInputComponent->BindAction(ForceImpulseUp, IE_Pressed, this, &AForceApplicationPawn::InteractPressedUp);
    

}

void AForceApplicationPawn::PerformInteraction()
{
    const FVector forward = GetActorForwardVector();
    UStaticMeshComponent* SM = Cast<UStaticMeshComponent>(this->MeshComponent);
    SM->AddImpulse(forward * SM->GetMass() * 1000);

}
void AForceApplicationPawn::PerformInteractionUp()
{
    const FVector forward = GetActorUpVector();
    UStaticMeshComponent* SM = Cast<UStaticMeshComponent>(this->MeshComponent);
    SM->AddImpulse(forward * SM->GetMass() * 1000);
    
}

