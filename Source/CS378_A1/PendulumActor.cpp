// Fill out your copyright notice in the Description page of Project Settings.


#include "PendulumActor.h"
#include "TimerManager.h"

// Sets default values
APendulumActor::APendulumActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	SwingComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SwingComponent"));
	SwingComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void APendulumActor::BeginPlay()
{
	Super::BeginPlay();
	GetWorldTimerManager().SetTimer(PendulumTimeManager, this, &APendulumActor::Swing, 1.0f, true, 1.0f);
	
}

// Called every frame
void APendulumActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APendulumActor::Swing() {
	const FVector forward = GetActorForwardVector();
	UStaticMeshComponent* SM = Cast<UStaticMeshComponent>(this->SwingComponent);
	if (swings % 2 == 0) {
		SM->AddImpulse(forward * SM->GetMass() * 600);
	}
	else {
		SM->AddImpulse(forward * SM->GetMass() * 600 * -1);
	}
	swings++;
}

