// Fill out your copyright notice in the Description page of Project Settings.

#include "TimerManager.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/CollisionProfile.h"
#include "Engine/StaticMesh.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "GameFramework/Actor.h"

#include "TeleportActor.h"

// Sets default values
ATeleportActor::ATeleportActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    //Components
    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
    RootComponent = MeshComponent;


}

// Called when the game starts or when spawned
void ATeleportActor::BeginPlay()
{
	Super::BeginPlay();
    
    GetWorldTimerManager().SetTimer(TeleportTimerHandler, this, &ATeleportActor::Teleport, 7.0f, true, 7.0f);
	
}

// Called every frame
void ATeleportActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    
    

}
void ATeleportActor::Teleport()
{
    
    FVector loc = this->GetActorLocation();
    if (GEngine)
    {
     GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Blue, TEXT("TeleportActor Teleport"));
     GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Yellow, FString::Printf(TEXT("Player Location: %s"), *loc.ToString()));
    }
    if(teleportPad)
    {
        FVector tploc = teleportPad->GetActorLocation();
        this->SetActorLocation(tploc);
        teleportPad->SetActorLocation(loc);
    }
//
//    const FVector forward = GetActorForwardVector();
//    UStaticMeshComponent* SM = Cast<UStaticMeshComponent>(this->MeshComponent);
//    SM->AddImpulse(forward * SM->GetMass() * 1000);
}

