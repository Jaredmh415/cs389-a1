// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "MovingInteractable.generated.h"

UCLASS()
class CS378_A1_API AMovingInteractable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMovingInteractable();
    
    FORCEINLINE class UStaticMeshComponent *  GetMeshComponent() const {return MeshComponent;}
    
    UFUNCTION()
    void MoveInteractable();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UStaticMeshComponent *MeshComponent;
    FTimerHandle MovingTime;
    int direction;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
    

};
